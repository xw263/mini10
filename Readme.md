
[![pipeline status](https://gitlab.com/xw263/mini10/badges/main/pipeline.svg)](https://gitlab.com/xw263/mini10/-/commits/main)

# Mini Project 10

## Purpose
The goal of this project is to create a serverless AWS Lambda function using the Rust programming language and Cargo Lambda to deploy a local LLM inference endpoint.

## Inference Endpoint
https://hqcweovfxsaplslor4xk25catm0hnadg.lambda-url.us-east-1.on.aws/
![Screenshot_2024-04-13_at_7.49.44_PM](/uploads/39e126220ea86d617579cffd1b6088aa/Screenshot_2024-04-13_at_7.49.44_PM.png)


## Steps

#### Apply the model locally
1. Make a cargo lambda HTTP function `cargo lambda new <folder name>`
2. Choose a hugging face model available to use that works with rustformers (really only two choices as the model needs to be small enough to actually run on lambda): https://huggingface.co/rustformers
3. Modify the template with your own additions or deletions.
4. Add an inference endpoint when you use the rustfomers package (make sure to add my `Cargo.toml` dependencies)
5. Do `cargo lambda watch` to test locally.
6. Confirm your inference endpoint actually works. A rule of thumb is for every 2 second it takes to run the model locally it will take an addtional 1.5 minutes to run with your lambda function

#### Deploy on AWS
##### AWS IAM Web Management
7. Go to the AWS IAM web management console and add an IAM User for credentials.
Attach policies lambdafullaccess and iamfullaccess. Further instruction on AWS IAM is [here] (https://gitlab.com/xw263/mini_project_7)
8. source your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`. Add it to `.profile` in git repo. Add `.profile` to `.gitignore` in git repo
##### AWS ECR
9. Go to ECR in AWS and create a new private repository. If you have an existing one, feel free to delete it so you can incur less charges (no matter what this project will incur a charge due to the model itself being over 500mb way over the free tier limit)
10. Follow the instuctions
11. eplace step 2 with docker `buildx build --progress=plain --platform linux/arm64 -t <your image name here>`.
##### AWS Lambda
12. Once you have the image pushed, go to lambda, create new function, deploy with image, choose `arm64`
13. Wait for it to be fully deployed
14. Go to Configuration -> General Configuration -> edit -> change memory to the `max limit of 3008` (as new users can't go past that) -> change timeout to the max of `15 minutes` (as it takes the model a long time to compute)
15. Go back to Configuration -> Fucntion URL -> create a new function URL -> choose NONE not IAM -> keep BUFFERED -> enable CORS
16. Test your fucntion URL, it should take around 8 minutes to run if you use my model



## References
1. https://gitlab.com/jeremymtan/jeremytan-ids721-week10/-/tree/main?ref_type=heads
2. https://huggingface.co/rustformers/pythia-ggml
3. https://docs.github.com/en/repositories/working-with-files/managing-large-files/installing-git-large-file-storage
4. https://gitlab.com/xw263/mini_project_7

